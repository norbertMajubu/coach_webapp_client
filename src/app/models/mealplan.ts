export class Mealplan{
    name: string;
    energy: number;
    fat: number;
    carbs: number;
    protein: number;
    salt: number;
    price: number;
    img: string
}