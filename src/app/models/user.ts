import { Mealplan } from './mealplan';

export class User{
    username: string;
    firstname: string;
    surname: string;
    email: string;
    mealplan: Mealplan[] = [];
    water : number;
    steps : number;
    trainer_id : string;

    clients: string[] = [];

    role : number;
}