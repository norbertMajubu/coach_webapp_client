import {Ingredient} from './ingredient';
import { Preparationstep } from './preparationstep';

export class Recipe{
    name: string;
    meal: string;
    energy: number;
    fat: number;
    carbs: number;
    protein: number;
    salt: number;
    preparation: Preparationstep; //[] = [];
    dosed_ingredients: Ingredient; //[] = [];
    price: number;
    prep_time: number;
    img: string
}