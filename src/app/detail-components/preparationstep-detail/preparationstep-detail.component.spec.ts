import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationstepDetailComponent } from './preparationstep-detail.component';

describe('PreparationstepDetailComponent', () => {
  let component: PreparationstepDetailComponent;
  let fixture: ComponentFixture<PreparationstepDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparationstepDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationstepDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
