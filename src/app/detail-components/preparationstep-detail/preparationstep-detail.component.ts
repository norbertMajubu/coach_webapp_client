import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Preparationstep } from '../../models/preparationstep';
import { PreparationstepService } from '../../services/preparationstep.service';

@Component({
  selector: 'app-preparationstep-detail',
  templateUrl: './preparationstep-detail.component.html',
  styleUrls: [ './preparationstep-detail.component.css' ]
})

export class PreparationstepDetailComponent implements OnInit {
  @Input() preparationstep: Preparationstep;
  multiplePreparationstep: Preparationstep[];

  constructor(
    private route: ActivatedRoute,
    private preparationstepService: PreparationstepService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getPreparationstep();
  }
/*
  getPreparationstep(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.preparationstepService.getPreparationstep(name.toString())
      .subscribe(preparationstep => this.preparationstep =  preparationstep);/* {
        'name': 'bobi',
        'energy': 0,
        'fat': 0,
        'carbs':0,
        'protein': 0,
        'salt': 0,
        'price': 0,
        'img': 'jaaaj'
    });// preparationstep);
    console.log("after we got back: " + this.preparationstep)
}*/

  getPreparationstep(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.preparationstepService.getPreparationstep(name.toString())
      .subscribe(preparationstep => this.preparationstep = preparationstep);
  }

  searchPreparationstep(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.preparationstepService.searchPreparationstep(name.toString())
      .subscribe(preparationstep => this.multiplePreparationstep = preparationstep);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.preparationstepService.updatePreparationstep(this.preparationstep)
      .subscribe(() => this.goBack());
  }
}