import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: [ './recipe-detail.component.css' ]
})

export class RecipeDetailComponent implements OnInit {
  @Input() recipe: Recipe;
  multipleRecipe: Recipe[];

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipeService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getRecipe();
  }
/*
  getRecipe(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.recipeService.getRecipe(name.toString())
      .subscribe(recipe => this.recipe =  recipe);/* {
        'name': 'bobi',
        'energy': 0,
        'fat': 0,
        'carbs':0,
        'protein': 0,
        'salt': 0,
        'price': 0,
        'img': 'jaaaj'
    });// recipe);
    console.log("after we got back: " + this.recipe)
}*/

  getRecipe(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.recipeService.getRecipe(name.toString())
      .subscribe(recipe => this.recipe = recipe);
  }

  searchRecipe(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.recipeService.searchRecipe(name.toString())
      .subscribe(recipe => this.multipleRecipe = recipe);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.recipeService.updateRecipe(this.recipe)
      .subscribe(() => this.goBack());
  }
}