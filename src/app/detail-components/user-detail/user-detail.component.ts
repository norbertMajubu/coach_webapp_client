import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [ './user-detail.component.css' ]
})

export class UserDetailComponent implements OnInit {
  @Input() user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    const name = this.route.snapshot.paramMap.get('username');
    console.log("value in detail ctonroller: " + name);
    this.userService.getUser(name.toString())
      .subscribe(user => this.user = user);
  }
  

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.userService.updateUser(this.user)
      .subscribe(() => this.goBack());
  }
}