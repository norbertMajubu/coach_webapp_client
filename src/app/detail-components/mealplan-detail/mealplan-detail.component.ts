import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Mealplan } from '../../models/mealplan';
import { MealplanService } from '../../services/mealplan.service';

@Component({
  selector: 'app-mealplan-detail',
  templateUrl: './mealplan-detail.component.html',
  styleUrls: [ './mealplan-detail.component.css' ]
})

export class MealplanDetailComponent implements OnInit {
  @Input() mealplan: Mealplan;
  multipleMealplan: Mealplan[];

  constructor(
    private route: ActivatedRoute,
    private mealplanService: MealplanService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getMealplan();
  }
/*
  getMealplan(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.mealplanService.getMealplan(name.toString())
      .subscribe(mealplan => this.mealplan =  mealplan);/* {
        'name': 'bobi',
        'energy': 0,
        'fat': 0,
        'carbs':0,
        'protein': 0,
        'salt': 0,
        'price': 0,
        'img': 'jaaaj'
    });// mealplan);
    console.log("after we got back: " + this.mealplan)
}*/

  getMealplan(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.mealplanService.getMealplan(name.toString())
      .subscribe(mealplan => this.mealplan = mealplan);
  }

  searchMealplan(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.mealplanService.searchMealplan(name.toString())
      .subscribe(mealplan => this.multipleMealplan = mealplan);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.mealplanService.updateMealplan(this.mealplan)
      .subscribe(() => this.goBack());
  }
}