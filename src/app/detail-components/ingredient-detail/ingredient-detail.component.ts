import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';

@Component({
  selector: 'app-ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: [ './ingredient-detail.component.css' ]
})

export class IngredientDetailComponent implements OnInit {
  @Input() ingredient: Ingredient;
  multipleIngredient: Ingredient[];

  constructor(
    private route: ActivatedRoute,
    private ingredientService: IngredientService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getIngredient();
  }
/*
  getIngredient(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.ingredientService.getIngredient(name.toString())
      .subscribe(ingredient => this.ingredient =  ingredient);/* {
        'name': 'bobi',
        'energy': 0,
        'fat': 0,
        'carbs':0,
        'protein': 0,
        'salt': 0,
        'price': 0,
        'img': 'jaaaj'
    });// ingredient);
    console.log("after we got back: " + this.ingredient)
}*/

  getIngredient(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.ingredientService.getIngredient(name.toString())
      .subscribe(ingredient => this.ingredient = ingredient);
  }

  searchIngredient(): void {
    const name = this.route.snapshot.paramMap.get('name');
    console.log("value in detail ctonroller: " + name);
    this.ingredientService.searchIngredient(name.toString())
      .subscribe(ingredient => this.multipleIngredient = ingredient);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.ingredientService.updateIngredient(this.ingredient)
      .subscribe(() => this.goBack());
  }
}