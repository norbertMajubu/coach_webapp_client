import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Ingredient } from '../models/ingredient';
import { MessageService } from '../message.service';

import { httpOptions, httpOptionsJson} from './service_headers'
import {serverUrl} from './config'

@Injectable()
export class IngredientService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET Ingredients from the server */
  /*getIngredients (): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>(serverUrl)
      .pipe(
        tap(Ingredients => this.log(`fetched Ingredients`)),
        catchError(this.handleError('getIngredients', []))
      );
  }*/
  getIngredients (): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>(serverUrl + 'getingredients', httpOptions)
      .pipe(
        catchError(this.handleError('getIngredients', []))
      );
  }

  /** GET Ingredient by name. Return `undefined` when name not found */
  getIngredientNo404<Data>(name: string): Observable<Ingredient> {
    const url = `${serverUrl}/getingredients?name=${name}`;
    return this.http.get<Ingredient[]>(url, httpOptions)
      .pipe(
        map(Ingredients => Ingredients[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} Ingredient name=${name}`);
        }),
        catchError(this.handleError<Ingredient>(`getIngredient name=${name}`))
      );
  }

  /** GET Ingredient by name. Will 404 if name not found */
  /*getIngredient(name: string): Observable<Ingredient> {
    const url = `${serverUrl + 'getingredients'}/${name}`;
    var retVal = this.http.get<Ingredient>(url, httpOptionsJson)
      .pipe(tap(_ => this.log(`fetched ingredient name=${name}`)),
      catchError(this.handleError<Ingredient>(`getIngredient name=${name}`))
    );
    console.log("retval: " + retVal);
    return retVal;
  }*/

  /** GET ingredient by id. Will 404 if id not found */
  /*getIngredient(name: number): Observable<Ingredient> {
    const url = `${serverUrl + 'getingredients'}/${name}`;
    return this.http.get<Ingredient>(url, httpOptionsJson).pipe(
      tap(_ => this.log(`fetched ingredient id=${name}`)),
      catchError(this.handleError<Ingredient>(`getIngredient id=${name}`))
    );
  }
*/

    /** GET Ingredient by name. Will 404 if name not found */
  getIngredient(name: string): Observable<Ingredient> {
    const url = `${serverUrl + 'getingredients'}/${name}`;
    return this.http.get<Ingredient>(url, httpOptions)
      .pipe(tap(_ => this.log(`fetched Ingredient name=${name}`)),
      catchError(this.handleError<Ingredient>(`getIngredient name=${name}`))
    );
  }

  /* GET Ingredients whose name contains search term */
  searchIngredients(term: string): Observable<Ingredient[]> {
    if (!term.trim()) {
      // if not search term, return empty Ingredient array.
      return of([]);
    }
    console.log("we are searching for: "+ term)
    return this.http.get<Ingredient[]>(serverUrl + `searchingredient/${term}`, httpOptions).pipe(
      tap(_ => this.log(`found Ingredients matching "${term}"`)),
      catchError(this.handleError<Ingredient[]>('searchIngredients', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new Ingredient to the server */
  addIngredient (ingredient: Ingredient): Observable<Ingredient> {
    return this.http.post<Ingredient>(serverUrl + 'ingredient/create', ingredient, httpOptionsJson).pipe(
      tap((ingredient: Ingredient) => this.log(`added ingredient w/ name=${ingredient.name}`)),
      catchError(this.handleError<Ingredient>('addIngredient'))
    );
  }

  /** DELETE: delete the Ingredient from the server */
  deleteIngredient (ingredient: Ingredient | string): Observable<Ingredient> {
    const name = typeof ingredient === 'string' ? ingredient : ingredient.name;
    const url = `${serverUrl + 'ingredient/delete'}/${name}`;

    return this.http.post<Ingredient>(url, ingredient, httpOptionsJson).pipe(
      tap(_ => this.log(`deleted ingredient name=${name}`)),
      catchError(this.handleError<Ingredient>('deleteIngredient'))
    );
  }

  /** PUT: update the Ingredient on the server */
 /* deleteIngredient (ingredient: Ingredient): Observable<any> {
    return this.http.post(`${serverUrl}ingredient/delete/${ingredient.name}`, ingredient, httpOptions).pipe(
      tap(_ => this.log(`updated ingredient name=${ingredient.name}`)),
      catchError(this.handleError<any>('updateIngredient'))
    );
  }
*/

  /** PUT: update the Ingredient on the server */
  updateIngredient (ingredient: Ingredient): Observable<any> {
    return this.http.put(`${serverUrl}ingredient/update/${ingredient.name}`, ingredient, httpOptionsJson).pipe(
      tap(_ => this.log(`updated ingredient name=${ingredient.name}`)),
      catchError(this.handleError<any>('updateIngredient'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a IngredientService message with the MessageService */
  private log(message: string) {
    this.messageService.add('IngredientService: ' + message);
  }
}