//TODO: INSTEAD OF THIS CONFIG FILE RELY ON THIS LATER:
//https://www.jvandemo.com/how-to-configure-your-angularjs-application-using-environment-variables/


const serverUrl = 'http://localhost:8081/'; 
//'https://peaceful-headland-46268.herokuapp.com/';
//'http://localhost:8081/'; 
//'https://peaceful-headland-46268.herokuapp.com/';
const localPort = 'http://localhost:8080/';
//'https://shrouded-brook-82106.herokuapp.com/';
//'https://sheltered-wildwood-62442.herokuapp.com/';
//'http://localhost:8080/';
export {serverUrl, localPort};