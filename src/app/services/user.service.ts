import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from '../models/user';
import { MessageService } from '../message.service';

import { httpOptions, httpOptionsJson} from './service_headers'

import {serverUrl} from './config'

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET Users from the server */
  /*getUsers (): Observable<User[]> {
    return this.http.get<User[]>(serverUrl)
      .pipe(
        tap(Users => this.log(`fetched Users`)),
        catchError(this.handleError('getUsers', []))
      );
  }*/

  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(serverUrl + 'getusers', httpOptions)
      .pipe(
        catchError(this.handleError('getUsers', []))
      );
  }


  /** GET User by username. Return `undefined` when username not found */
  getUserNo404<Data>(username: string): Observable<User> {
    
    const url = `${serverUrl}/?username=${username}`;
    console.log("urlke: " + url)
    return this.http.get<User[]>(url, httpOptions)
      .pipe(
        map(Users => Users[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} User username=${username}`);
        }),
        catchError(this.handleError<User>(`getUser username=${username}`))
      );
  }

  /** GET User by username. Will 404 if username not found */
  getUser(username: string): Observable<User> {
    const url = `${serverUrl + 'getuser'}/${username}`;
    return this.http.get<User>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched User username=${username}`)),
      catchError(this.handleError<User>(`getUser username=${username}`))
    );
  }

  /* GET Users whose username contains search term */
  searchUsers(term: string): Observable<User[]> {
    if (!term.trim()) {
      // if not search term, return empty User array.
      return of([]);
    }
    return this.http.get<User[]>(serverUrl + `searchusers/${term}`,  httpOptions).pipe(
      tap(_ => this.log(`found Users matching "${term}"`)),
      catchError(this.handleError<User[]>('searchUsers', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new User to the server */
  addUser (users: User): Observable<User> {
    return this.http.post<User>(serverUrl + 'user/create', users, httpOptionsJson).pipe(
      tap((users: User) => this.log(`added user w/ username=${users.username}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  /** DELETE: delete the User from the server */
  deleteUser (user: User | string): Observable<User> {
    const username = typeof user === 'string' ? user : user.username;
    //const username = typeof user === 'string' ? user : user.username;
    const url = `${serverUrl}user/delete/${username}`;

    return this.http.post<User>(url, user, httpOptionsJson).pipe(
      tap(_ => this.log(`deleted user username=${username}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  /** PUT: update the User on the server */
  updateUser (user: User): Observable<any> {
    return this.http.put(`${serverUrl}user/update/${user.username}`, user, httpOptionsJson).pipe(
      tap(_ => this.log(`updated user username=${user.username}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - username of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    this.messageService.add('UserService: ' + message);
  }
}