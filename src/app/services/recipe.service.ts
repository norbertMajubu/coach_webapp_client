import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Recipe } from '../models/recipe';
import { MessageService } from '../message.service';

import { httpOptions, httpOptionsJson} from './service_headers'

import {serverUrl} from './config'

@Injectable()
export class RecipeService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET Recipes from the server */
  /*getRecipes (): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(serverUrl)
      .pipe(
        tap(Recipes => this.log(`fetched Recipes`)),
        catchError(this.handleError('getRecipes', []))
      );
  }*/

  getRecipes (): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(serverUrl + 'getrecipes', httpOptions)
      .pipe(
        catchError(this.handleError('getRecipes', []))
      );
  }


  /** GET Recipe by name. Return `undefined` when name not found */
  getRecipeNo404<Data>(name: string): Observable<Recipe> {
    const url = `${serverUrl}/?name=${name}`;
    return this.http.get<Recipe[]>(url, httpOptions)
      .pipe(
        map(Recipes => Recipes[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} Recipe name=${name}`);
        }),
        catchError(this.handleError<Recipe>(`getRecipe name=${name}`))
      );
  }

  /** GET Recipe by name. Will 404 if name not found */
  getRecipe(name: string): Observable<Recipe> {
    console.log("na dicsak: " + name)
    const url = `${serverUrl + 'getrecipe'}/${name}`;
    return this.http.get<Recipe>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched Recipe name=${name}`)),
      catchError(this.handleError<Recipe>(`getRecipe name=${name}`))
    );
  }

  /* GET Recipes whose name contains search term */
  searchRecipes(term: string): Observable<Recipe[]> {
    if (!term.trim()) {
      // if not search term, return empty Recipe array.
      return of([]);
    }
    return this.http.get<Recipe[]>(serverUrl + `searchrecipes/${term}`, httpOptions).pipe(
      tap(_ => this.log(`found Recipes matching "${term}"`)),
      catchError(this.handleError<Recipe[]>('searchRecipes', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new Recipe to the server */
  addRecipe (recipes: Recipe): Observable<Recipe> {
    console.log("na whatever: "+ recipes.fat)
    return this.http.post<Recipe>(serverUrl + 'recipe/create', recipes, httpOptionsJson).pipe(
      tap((recipes: Recipe) => this.log(`added recipe w/ name=${recipes.name}`)),
      catchError(this.handleError<Recipe>('addRecipe'))
    );
  }

  /** DELETE: delete the Recipe from the server */
  deleteRecipe (recipe: Recipe | string): Observable<Recipe> {
    const name = typeof recipe === 'string' ? recipe : recipe.name;
    const url = `${serverUrl}recipe/delete/${name}`;

    return this.http.post<Recipe>(url, recipe, httpOptions).pipe(
      tap(_ => this.log(`deleted recipe name=${name}`)),
      catchError(this.handleError<Recipe>('deleteRecipe'))
    );
  }

  /** PUT: update the Recipe on the server */
  updateRecipe (recipe: Recipe): Observable<any> {
    return this.http.put(`${serverUrl}recipe/${recipe.name}/update`, recipe, httpOptionsJson).pipe(
      tap(_ => this.log(`updated recipe name=${recipe.name}`)),
      catchError(this.handleError<any>('updateRecipe'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a RecipeService message with the MessageService */
  private log(message: string) {
    this.messageService.add('RecipeService: ' + message);
  }
}