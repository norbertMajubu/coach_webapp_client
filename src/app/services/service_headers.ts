import { HttpHeaders } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
       'Authorization': 'Bearer ' + localStorage.getItem('access_token')
     }), 
  };
  
  const httpOptionsJson = {
    headers: new HttpHeaders({
       'Content-Type': 'application/json',
       'Authorization': 'Bearer ' + localStorage.getItem('access_token')
     }), 
  };
  

  export {httpOptions, httpOptionsJson}