import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Mealplan } from '../models/mealplan';
import { MessageService } from '../message.service';

import { httpOptions, httpOptionsJson} from './service_headers'

import {serverUrl} from './config'

@Injectable()
export class MealplanService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET Mealplans from the server */
  /*getMealplans (): Observable<Mealplan[]> {
    return this.http.get<Mealplan[]>(serverUrl)
      .pipe(
        tap(Mealplans => this.log(`fetched Mealplans`)),
        catchError(this.handleError('getMealplans', []))
      );
  }*/

  getMealplans (): Observable<Mealplan[]> {
    return this.http.get<Mealplan[]>(serverUrl + 'getmealplans', httpOptions)
      .pipe(
        catchError(this.handleError('getMealplans', []))
      );
  }


  /** GET Mealplan by name. Return `undefined` when name not found */
  getMealplanNo404<Data>(name: string): Observable<Mealplan> {
    const url = `${serverUrl}/?name=${name}`;
    return this.http.get<Mealplan[]>(url, httpOptions)
      .pipe(
        map(Mealplans => Mealplans[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} Mealplan name=${name}`);
        }),
        catchError(this.handleError<Mealplan>(`getMealplan name=${name}`))
      );
  }

  /** GET Mealplan by name. Will 404 if name not found */
  getMealplan(name: string): Observable<Mealplan> {
    const url = `${serverUrl + 'getmealplan'}/${name}`;
    return this.http.get<Mealplan>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched Mealplan name=${name}`)),
      catchError(this.handleError<Mealplan>(`getMealplan name=${name}`))
    );
  }

  /* GET Mealplans whose name contains search term */
  searchMealplans(term: string): Observable<Mealplan[]> {
    if (!term.trim()) {
      // if not search term, return empty Mealplan array.
      return of([]);
    }
    return this.http.get<Mealplan[]>(`api/Mealplans/?name=${term}`, httpOptions).pipe(
      tap(_ => this.log(`found Mealplans matching "${term}"`)),
      catchError(this.handleError<Mealplan[]>('searchMealplans', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new Mealplan to the server */
  addMealplan (mealplan: Mealplan): Observable<Mealplan> {
    return this.http.post<Mealplan>(serverUrl + 'mealplan/create', mealplan, httpOptionsJson).pipe(
      tap((mealplan: Mealplan) => this.log(`added mealplan w/ name=${mealplan.name}`)),
      catchError(this.handleError<Mealplan>('addMealplan'))
    );
  }

  /** DELETE: delete the Mealplan from the server */
  deleteMealplan (mealplan: Mealplan | string): Observable<Mealplan> {
    const name = typeof mealplan === 'string' ? mealplan : mealplan.name;
    const url = `${serverUrl}mealplan/delete/${name}`;

    return this.http.post<Mealplan>(url, mealplan, httpOptionsJson).pipe(
      tap(_ => this.log(`deleted Mealplan name=${name}`)),
      catchError(this.handleError<Mealplan>('deleteMealplan'))
    );
  }

  /** PUT: update the Mealplan on the server */
  updateMealplan (mealplan: Mealplan): Observable<any> {
    return this.http.put(`${serverUrl}mealplan/update/${mealplan.name}`, mealplan, httpOptionsJson).pipe(
      tap(_ => this.log(`updated mealplan name=${mealplan.name}`)),
      catchError(this.handleError<any>('updateMealplan'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a MealplanService message with the MessageService */
  private log(message: string) {
    this.messageService.add('MealplanService: ' + message);
  }
}