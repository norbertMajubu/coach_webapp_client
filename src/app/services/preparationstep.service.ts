import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Preparationstep } from '../models/preparationstep';
import { MessageService } from '../message.service';

import { httpOptions, httpOptionsJson} from './service_headers'

import {serverUrl} from './config'

@Injectable()
export class PreparationstepService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET Preparationsteps from the server */
  /*getPreparationsteps (): Observable<Preparationstep[]> {
    return this.http.get<Preparationstep[]>(serverUrl)
      .pipe(
        tap(Preparationsteps => this.log(`fetched Preparationsteps`)),
        catchError(this.handleError('getPreparationsteps', []))
      );
  }*/

  getPreparationsteps (): Observable<Preparationstep[]> {
    return this.http.get<Preparationstep[]>(serverUrl + 'getpreparationsteps', httpOptions)
      .pipe(
        catchError(this.handleError('getPreparationsteps', []))
      );
  }


  /** GET Preparationstep by name. Return `undefined` when name not found */
  getPreparationstepNo404<Data>(name: string): Observable<Preparationstep> {
    const url = `${serverUrl}/?name=${name}`;
    return this.http.get<Preparationstep[]>(url, httpOptions)
      .pipe(
        map(Preparationsteps => Preparationsteps[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} Preparationstep name=${name}`);
        }),
        catchError(this.handleError<Preparationstep>(`getPreparationstep name=${name}`))
      );
  }

  /** GET Preparationstep by name. Will 404 if name not found */
  getPreparationstep(name: string): Observable<Preparationstep> {
    console.log("na dicsak: " + name)
    const url = `${serverUrl + 'getpreparationstep'}/${name}`;
    return this.http.get<Preparationstep>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched Preparationstep name=${name}`)),
      catchError(this.handleError<Preparationstep>(`getPreparationstep name=${name}`))
    );
  }

  /* GET Preparationsteps whose name contains search term */
  searchPreparationsteps(term: string): Observable<Preparationstep[]> {
    if (!term.trim()) {
      // if not search term, return empty Preparationstep array.
      return of([]);
    }
    return this.http.get<Preparationstep[]>(serverUrl + `searchpreparationsteps/${term}`, httpOptions).pipe(
      tap(_ => this.log(`found Preparationsteps matching "${term}"`)),
      catchError(this.handleError<Preparationstep[]>('searchPreparationsteps', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new Preparationstep to the server */
  addPreparationstep (preparationstep: Preparationstep): Observable<Preparationstep> {
    console.log("na whatever: "+ preparationstep.name)
    return this.http.post<Preparationstep>(serverUrl + 'preparationstep/create', preparationstep, httpOptionsJson).pipe(
      tap((preparationstep: Preparationstep) => this.log(`added preparationstep w/ name=${preparationstep.name}`)),
      catchError(this.handleError<Preparationstep>('addPreparationstep'))
    );
  }

  /** DELETE: delete the Preparationstep from the server */
  deletePreparationstep (preparationstep: Preparationstep | string): Observable<Preparationstep> {
    const name = typeof preparationstep === 'string' ? preparationstep : preparationstep.name;
    const url = `${serverUrl}preparationstep/delete/${name}`;

    return this.http.post<Preparationstep>(url,preparationstep, httpOptionsJson).pipe(
      tap(_ => this.log(`deleted preparationstep name=${name}`)),
      catchError(this.handleError<Preparationstep>('deletePreparationstep'))
    );
  }


  /** PUT: update the Preparationstep on the server */
  updatePreparationstep (preparationstep: Preparationstep): Observable<any> {
    return this.http.put(`${serverUrl}/preparationstep/update/${preparationstep.name}`, preparationstep, httpOptionsJson).pipe(
      tap(_ => this.log(`updated preparationstep name=${preparationstep.name}`)),
      catchError(this.handleError<any>('updatePreparationstep'))
    );
  }
  Preparationstep
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a PreparationstepService message with the MessageService */
  private log(message: string) {
    this.messageService.add('PreparationstepService: ' + message);
  }
}