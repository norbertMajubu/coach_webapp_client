// src/app/auth/auth.service.ts

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import * as auth0 from 'auth0-js';
import {localPort} from './config'


@Injectable()
export class AuthService {

  auth0 = new auth0.WebAuth({
    clientID: 'w8CDKeJPT3uVhsYpjVoJhWicEdCrvgp7',
    domain: 'metasweat.eu.auth0.com',
    responseType: 'token id_token',
    audience: 'metasweat_api', //'https://metasweat.eu.auth0.com/userinfo',
    redirectUri: localPort,
    scope: 'trainer',
  });

  constructor(
    public router: Router
  ) {}

  public login(): void {
    this.auth0.authorize();   
  }

 // ...window.location.hash.split('=')[1].split('&')[0]
 public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/dashboard']);
        window.location.reload();
      } else if (err) {
        this.router.navigate(['/dashboard']);
        console.log(err);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the Access Token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // Go back to the home route
    this.router.navigate(['/']);
    window.location.reload();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // Access Token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  
}