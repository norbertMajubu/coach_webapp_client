import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngredientsComponent } from './components/ingredients/ingredients.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PreparationstepsComponent } from './components/preparationsteps/preparationsteps.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { UsersComponent } from './components/users/users.component';
import { MealplansComponent } from './components/mealplans/mealplans.component';
import { IngredientDetailComponent } from './detail-components/ingredient-detail/ingredient-detail.component';
import { UserDetailComponent } from './detail-components/user-detail/user-detail.component';
import { PreparationstepDetailComponent } from './detail-components/preparationstep-detail/preparationstep-detail.component';
import { MealplanDetailComponent } from './detail-components/mealplan-detail/mealplan-detail.component';
import { RecipeDetailComponent } from './detail-components/recipe-detail/recipe-detail.component';

const routes: Routes = [
  { path: 'ingredients', component: IngredientsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'preparationsteps', component: PreparationstepsComponent },
  { path: 'recipes', component: RecipesComponent },
  { path: 'users', component: UsersComponent },
  { path: 'detail/ingredient/:name', component: IngredientDetailComponent },
  { path: 'detail/user/:username', component: UserDetailComponent },
  { path: 'detail/recipe/:name', component: RecipeDetailComponent },
  { path: 'detail/preparationstep/:name', component: PreparationstepDetailComponent },
  { path: 'detail/mealplan/:name', component: MealplanDetailComponent },
  { path: 'user_detail/:username', component: UserDetailComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'api_auth', redirectTo:  '/dashboard', pathMatch: 'full' },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}