import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationstepSearchComponent } from './preparationstep-search.component';

describe('PreparationstepSearchComponent', () => {
  let component: PreparationstepSearchComponent;
  let fixture: ComponentFixture<PreparationstepSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparationstepSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationstepSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
