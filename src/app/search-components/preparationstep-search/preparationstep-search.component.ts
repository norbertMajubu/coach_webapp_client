import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { of }         from 'rxjs/observable/of';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Preparationstep } from '../../models/preparationstep';
import { PreparationstepService } from '../../services/preparationstep.service';

@Component({
  selector: 'app-preparationstep-search',
  templateUrl: './preparationstep-search.component.html',
  styleUrls: [ './preparationstep-search.component.css' ]
})
export class PreparationstepSearchComponent implements OnInit {
  preparationstep$: Observable<Preparationstep[]>;
  private searchTerms = new Subject<string>();

  constructor(private preparationstepService: PreparationstepService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.preparationstep$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.preparationstepService.searchPreparationsteps(term)),
    );
  }
}