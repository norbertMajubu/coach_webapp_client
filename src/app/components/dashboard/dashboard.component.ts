import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  ingredient: Ingredient[] = [];

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {
    this.getIngredientes();
  }

  getIngredientes(): void {
    this.ingredientService.getIngredients()
      .subscribe(ingredient => this.ingredient = ingredient.slice(1, 5));
  }
}