import { Component, OnInit } from '@angular/core';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-Recipe',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipes: Recipe[];

  constructor(private recipesService: RecipeService) { }

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes(): void {
    this.recipesService.getRecipes()
    .subscribe(recipes => this.recipes = recipes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.recipesService.addRecipe({
      'name': name,
      'energy':0,
      'fat':0,
      'carbs':0,
      'protein':0,
      'salt':0,
      'price':0,
      'img':''
  } as Recipe)
      .subscribe(recipes => {
        this.recipes.push(recipes);
      });
  }

  delete(recipes: Recipe): void {
    this.recipes = this.recipes.filter(h => h !== recipes);
    this.recipesService.deleteRecipe(recipes).subscribe();
  }

}