import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { EmailValidator } from '@angular/forms';

@Component({
  selector: 'app-User',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];

  constructor(private usersService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.usersService.getUsers()
    .subscribe(users => this.users = users);
  }

  add(
    username: string,
    firstname:string,
    surname: string,
    email: string,    
  ): void {
    username = username.trim();
    firstname= firstname.trim();
    surname= surname.trim();
    email= email.trim();
    if (!username) { return; }
    this.usersService.addUser({
      'username': username,
      'firstname': firstname,
      'surname': surname,
      'email': email,
      'mealplan': [],
      'water' : 0,
      'steps' : 0,
      'trainer_id' : '',
      'clients': [],
      'role' : 0
  } as User)
      .subscribe(users => {
        this.users.push(users);
      });
  }

  delete(users: User): void {
    this.users = this.users.filter(h => h !== users);
    this.usersService.deleteUser(users).subscribe();
  }

}