import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../../models/ingredient';
import { IngredientService } from '../../services/ingredient.service';

@Component({
  selector: 'app-Ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent implements OnInit {
  ingredient: Ingredient[];

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {
    this.getIngredient();
  }

  getIngredient(): void {
    this.ingredientService.getIngredients()
    .subscribe(ingredient => this.ingredient = ingredient);
  }

  add(
    name: string,
    energy: number,
    fat: number,
    carbs: number,
    protein: number,
    salt: number,
    price: number,
    img: string  
    ): void {
    name = name.trim();
    img = img.trim();
    if (!name) { return; }
    this.ingredientService.addIngredient({
      'name': name,
      'energy': energy,
      'fat': fat,
      'carbs':carbs,
      'protein': protein,
      'salt': salt,
      'price': price,
      'img': img
  } as Ingredient)
      .subscribe(ingredient => {
        this.ingredient.push(ingredient);
      });
  }

  delete(ingredient: Ingredient): void {
    this.ingredient = this.ingredient.filter(h => h !== ingredient);
    this.ingredientService.deleteIngredient(ingredient).subscribe();
  }

}