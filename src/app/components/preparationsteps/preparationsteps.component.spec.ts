import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationstepsComponent } from './preparationsteps.component';

describe('PreparationstepComponent', () => {
  let component: PreparationstepsComponent;
  let fixture: ComponentFixture<PreparationstepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparationstepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationstepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
