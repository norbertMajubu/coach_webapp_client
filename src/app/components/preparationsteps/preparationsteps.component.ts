import { Component, OnInit } from '@angular/core';

import { Preparationstep } from '../../models/preparationstep';
import { PreparationstepService } from '../../services/preparationstep.service';

@Component({
  selector: 'app-Preparationsteps',
  templateUrl: './preparationsteps.component.html',
  styleUrls: ['./preparationsteps.component.css']
})
export class PreparationstepsComponent implements OnInit {
  preparationstep: Preparationstep[];

  constructor(private preparationstepService: PreparationstepService) { }

  ngOnInit() {
    this.getPreparationstep();
  }

  getPreparationstep(): void {
    this.preparationstepService.getPreparationsteps()
    .subscribe(preparationstep => this.preparationstep = preparationstep);
  }

  add(name: string, equipment_needed: string, img: string ): void {
    name = name.trim();
    if (!name) { return; }
    this.preparationstepService.addPreparationstep({
      '_id':'', 
      'name': name,
      'equipment_needed': equipment_needed,
      'img': img,
  } as Preparationstep)
      .subscribe(preparationstep => {
        this.preparationstep.push(preparationstep);
      });
  }

  delete(preparationstep: Preparationstep): void {
    this.preparationstep = this.preparationstep.filter(h => h !== preparationstep);
    this.preparationstepService.deletePreparationstep(preparationstep).subscribe();
  }

}