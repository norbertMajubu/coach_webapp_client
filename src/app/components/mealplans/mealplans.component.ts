import { Component, OnInit } from '@angular/core';

import { Mealplan } from '../../models/mealplan';
import { MealplanService } from '../../services/mealplan.service';

@Component({
  selector: 'app-Mealplan',
  templateUrl: './mealplans.component.html',
  styleUrls: ['./mealplans.component.css']
})
export class MealplansComponent implements OnInit {
  mealplan: Mealplan[];

  constructor(private mealplanService: MealplanService) { }

  ngOnInit() {
    this.getMealplan();
  }

  getMealplan(): void {
    this.mealplanService.getMealplans()
    .subscribe(mealplan => this.mealplan = mealplan);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.mealplanService.addMealplan({
      'name': name,
      'energy':0,
      'fat':0,
      'carbs':0,
      'protein':0,
      'salt':0,
      'price':0,
      'img':''
  } as Mealplan)
      .subscribe(mealplan => {
        this.mealplan.push(mealplan);
      });
  }

  delete(mealplan: Mealplan): void {
    this.mealplan = this.mealplan.filter(h => h !== mealplan);
    this.mealplanService.deleteMealplan(mealplan).subscribe();
  }

}