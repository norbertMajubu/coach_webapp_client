import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { IngredientDetailComponent } from './detail-components/ingredient-detail/ingredient-detail.component';
import { UserDetailComponent } from './detail-components/user-detail/user-detail.component';
import { PreparationstepDetailComponent } from './detail-components/preparationstep-detail/preparationstep-detail.component';
import { MealplanDetailComponent } from './detail-components/mealplan-detail/mealplan-detail.component';
import { RecipeDetailComponent } from './detail-components/recipe-detail/recipe-detail.component';


import { IngredientsComponent } from './components/ingredients/ingredients.component';
import { PreparationstepsComponent } from './components/preparationsteps/preparationsteps.component';
import { MealplansComponent } from './components/mealplans/mealplans.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { UsersComponent } from './components/users/users.component';

import { IngredientSearchComponent } from './search-components/ingredient-search/ingredient-search.component';
import { UserSearchComponent } from './search-components/user-search/user-search.component';
import { RecipeSearchComponent } from './search-components/recipe-search/recipe-search.component';
import { PreparationstepSearchComponent } from './search-components/preparationstep-search/preparationstep-search.component';

import { IngredientService } from './services/ingredient.service';
import { MealplanService } from './services/mealplan.service';
import { PreparationstepService } from './services/preparationstep.service';
import { RecipeService } from './services/recipe.service';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';

import { MessageService } from './message.service';
import { MessagesComponent } from './messages/messages.component';



@NgModule({
  imports: [
 BrowserModule,
 FormsModule,
 AppRoutingModule,
 HttpClientModule,
  ],
  declarations: [
 AppComponent,

 DashboardComponent,
 IngredientsComponent,
 PreparationstepsComponent,
 MealplansComponent,
 RecipesComponent,
 UsersComponent,

 IngredientDetailComponent,
 UserDetailComponent,
 PreparationstepDetailComponent,
 MealplanDetailComponent,
 RecipeDetailComponent,
 MessagesComponent,
 IngredientSearchComponent,
 UserSearchComponent,
 RecipeSearchComponent,
 PreparationstepSearchComponent
  ],
  providers: [ IngredientService, MessageService, MealplanService, PreparationstepService, RecipeService, UserService, AuthService ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}